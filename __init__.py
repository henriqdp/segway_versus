import pyglet
import pymunk
from sys import argv

from game_elements.window import Game
from AI_controller import Learner


if __name__ == '__main__':
        space = pymunk.Space()

        if len(argv) < 2:
            print "Modo: "
            print "     python __init__.py <arquivo de pesos p1> <arquivo de pesos p2>"
            print "         arquivo de pesos: deve conter os pesos para o jogador pi"
        else:
            window = Game(space = space, run_pyglet = True, load1 = argv[1], load2 = argv[2], team1 = argv[3], team2 = argv[4])
            pyglet.app.run()
