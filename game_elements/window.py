# -*- coding: utf-8 -*-
import math, random, sys, signal, time, datetime

import AI_controller
import AI_controller_desafiante
import pyglet
from pyglet import font
from pyglet.gl import *

import pymunk
from .sprites import Cloud, Floor, GameObject, Rod
from . import constants
import numpy


ROUNDS = 10

G_VECTOR = (0.0, -900.0)
SKY_COLOR = (40.0/255, 185.0/255, 255/255)


def signal_handler(signal, frame):
        print(' Finalizando...')
        sys.exit(0)


#Classe que define a captação e as ações tomadas em eventos de mouse/teclado
class GameEventHandler(object):
    def __init__(self, window):
        self.window = window

    def on_key_press(self, symbol, modifiers):
        if symbol == pyglet.window.key.W:
            self.window.toggle_wind()
        if symbol == pyglet.window.key.ESCAPE:
            exit()
        return True

#classe que define o jogo
class Game(pyglet.window.Window):
    def __init__(self, space, run_pyglet, load1, load2, team1, team2):
        self.team = [team1, team2]

        space.gravity = (G_VECTOR[0], G_VECTOR[1])
        self.space = space
        self.epoch = 0

        if(run_pyglet):
            pyglet.window.Window.__init__(self, width = constants.W_WIDTH, height = constants.W_HEIGHT)
            self.wind = 0

        self.run_pyglet = run_pyglet

        self.push_handlers(GameEventHandler(self))
        self.batch_draw = pyglet.graphics.Batch()
        font.add_file('./resources/neon_pixel.ttf')
        neon_pixel = font.load('Neon Pixel-7')
        self.define_scenario()

        self.define_game_objects()

        signal.signal(signal.SIGINT, signal_handler)

        self.angular_velocity = 0
        pyglet.clock.schedule(self.update)
        self.current_iteration = 0

        self.learners = [AI_controller.Learner(self, load1), AI_controller_desafiante.Learner(self, load2)]
        self.scores = [0,0]
        self.angular_velocity = [0,0,0]
        self.pole_angle = [0,0,0]
        self.current_round = 1

        self.scores = [[], []]
        for player in range(0,2):
            for i in range(0, ROUNDS):
                self.scores[player].append(0)

    def define_scenario(self):
        background = pyglet.graphics.OrderedGroup(0)
        pyglet.gl.glClearColor(SKY_COLOR[0], SKY_COLOR[1], SKY_COLOR[2],1)
        self.clouds = (Cloud(batch = self.batch_draw,
                             x = constants.W_WIDTH * 0.2,
                             y = constants.W_HEIGHT * 0.8,
                             group = background),
                    Cloud(batch = self.batch_draw,
                          x = constants.W_WIDTH * 0.8,
                          y = constants.W_HEIGHT * 0.72,
                          group = background))
        self.floor = Floor(batch = self.batch_draw, space = self.space)


    def define_game_objects(self):
        self.players = [None, None]
        for player in range(0,2):
            self.players[player] = GameObject("wheel", batch = self.batch_draw,
                        space = self.space,
                        group = pyglet.graphics.OrderedGroup(1))
            self.players[player].initialize_player(player + 1)

        self.smileys = [None, None]
        for smiley in range(0,2):
            self.smileys[smiley] = GameObject("smiley", batch = self.batch_draw,
                                 space = self.space,
                                 group = pyglet.graphics.OrderedGroup(3), player = player)
            self.smileys[smiley].initialize_player(smiley + 1)
            self.smileys[smiley].obj_space.body.position.x = self.players[smiley].obj_space.body.position.x

        self.rods = [None, None]
        for rod in range(0,2):
            self.rods[rod] = pymunk.PinJoint(self.players[rod].obj_space.body,
                                  self.smileys[rod].obj_space.body)
            self.space.add(self.rods[rod])
            self.rods[rod] = Rod(batch = self.batch_draw, group = pyglet.graphics.OrderedGroup(2))
            self.smileys[rod].obj_space.body.apply_impulse((random.randint(-10000, 10000), 0))

    def on_draw(self):
        self.clear()
        for player in range(0,2):
            self.players[player].rotation = self.players[player].x - constants.W_WIDTH/2

        if self.current_round <= ROUNDS:
            self.batch_draw.draw()
            pyglet.text.Label('PLAYER 1: %3d' % self.scores[0][self.current_round-1],
                              font_name='Neon Pixel-7',
                              font_size=60,
                              x=250, y=self.height - 40,
                              anchor_x='center', anchor_y='center').draw()
            pyglet.text.Label('PLAYER 2: %3d' % self.scores[1][self.current_round-1],
                              font_name='Neon Pixel-7',
                              font_size=60,
                              x=constants.W_WIDTH - 250, y=self.height - 40,
                              anchor_x='center', anchor_y='center').draw()
        for i in range(0, int(math.ceil(constants.W_WIDTH / self.floor.img.width))):
            self.floor.img.blit(i * self.floor.img.width,0)



    def out_of_screen(self, player):
        if (self.smileys[player].obj_space.body.position.y <= constants.F_HEIGHT + self.smileys[player].obj_space.radius or
           self.players[player].obj_space.body.position.x <= 0 or self.players[player].obj_space.body.position.x  >= constants.W_WIDTH):
            return True
        else:
            return False


    def reset(self):
        for player in range(0,2):
             self.players[player].initialize_player(player+1)
             offset = random.randint(-constants.W_WIDTH/4, constants.W_WIDTH/4)
             self.players[player].obj_space.body.position.x += offset
             self.smileys[player].initialize_player(player+1)
             self.smileys[player].obj_space.body.position.x += offset

             self.smileys[player].obj_space.body.apply_impulse((random.randint(-15000, 15000), 0))
        self.current_iteration = 0

    def get_wheel_x(self):
        return self.lone_wheel.obj_space.body.position.x

    def get_smiley_y(self):
        return self.smiley.obj_space.body.position.y

    def get_pole_angle(self, player_no):
        if self.smileys[player_no].y == self.players[player_no].y:
            if self.smileys[player_no].x > self.players[player_no].x:
                arctan = 90
            else:
                arctan = -90
        else:
            if self.smileys[player_no].y > self.players[player_no].y:
                 tan = (self.smileys[player_no].x - self.players[player_no].x)/(self.smileys[player_no].y - self.players[player_no].y)
                 arctan = (math.atan(tan)*180)/math.pi
            else:
                 tan = (self.smileys[player_no].x - self.players[player_no].x)/(self.players[player_no].y - self.smileys[player_no].y)
                 arctan = 180 - (math.atan(tan)*180)/math.pi
        return arctan

    def get_pole_angular_velocity(self, player):
        return (self.get_pole_angle(player) - self.pole_angle[player])*60

    def toggle_visualization(self, visualize):
        self.visualize = visualize

    def wheel_impulse(self, impulse, player):
        self.players[player].obj_space.body.apply_impulse((impulse*5000, 0))

    def toggle_wind(self):
        if self.wind == 0:
            self.wind = random.randint(-500, 500)
        else:
            self.wind = 0
        print "Valor do vento: %d" % self.wind

    def exit(self):
        means  = [0.0, 0.0]
        stdevs = [0.0, 0.0]
        for player in range(0,2):
            for rodada in range(0, ROUNDS):
                means[player] += (self.scores[player][rodada])
                print "Pontuacao do jogador %d na rodada %d: %d" %(player, rodada, self.scores[player][rodada])

        for player in range(0,2):
            print "Media do equipe %s: %f" % (self.team[player], numpy.mean(self.scores[player]))
            for rodada in range(0, ROUNDS):
                stdevs[player] += (self.scores[player][rodada] - means[player])**2
            print "Desvio padrao da equipe %s: %f" % (self.team[player], numpy.std(self.scores[player]))

        if means[0] > means[1]:
            print("\n\n\nVENCEDOR: %s" % self.team[0])
        elif(means[1] > means[0]):
            print("\n\n\nVENCEDOR: %s" % self.team[1])
        elif(means[0] == means[1]):
            if(stdevs[0] > stdevs[1]):
                print("\n\n\nVENCEDOR: %s" % self.team[0])
            elif(stdevs[1] > stdevs[0]):
                print("\n\n\nVENCEDOR: %s" % self.team[1])
            else:
                print("\n\n\nEMPATE!")
        exit()

    def update(self, dt):
        if self.current_round > ROUNDS:
            self.exit()
        for player in range(0,2):
            self.players[player].update()
            self.smileys[player].update()
            self.rods[player].update(self, player)
            self.players[player].obj_space.body.velocity *= 0.98
            self.smileys[player].obj_space.body.velocity *= 0.98
            self.angular_velocity[player] = self.get_pole_angular_velocity(player)
            self.pole_angle[player] = self.get_pole_angle(player)
            if self.wind:
                self.players[player].obj_space.body.apply_impulse((self.wind, 0))
            if not self.out_of_screen(player):
                self.learners[player].step(self)
                self.scores[player][self.current_round-1] += 1

        self.current_iteration += 1

        if(self.current_iteration > constants.TIMEOUT_VERSUS) or self.out_of_screen(0) and self.out_of_screen(1):
            self.reset()
            self.current_round += 1

        for learner in range(0,2):
            self.learners[learner].step(self)
        self.space.step(dt)
